### Configure GitLab connection via public SSH Key

#### Add the following into `.ssh/config`

```bash
Host gitlab.com
  HostName gitlab.com
  User git
  IdentityFile ~/.ssh/id_rsa.gitlab
  IdentitiesOnly yes
```

#### Generate SSH Key Pair for GitLab

```bash
$ ssh-keygen -f ~/.ssh/id_rsa.gitlab -t rsa
```

Add public SSH key into GitLab repository `~/.ssh/id_rsa.gitlab.pub`


